# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 21:56:05 2021

@author: 王可名
"""

import numpy as np
import matplotlib.pylab as plt

#the define part, in this part all of the defined functions are given

#to define a function to get the group of y
def y_group(N,variable):
    a=variable['y_min']
    b=variable['y_max']
    y=np.zeros(N+1)
    for i in range(N+1):
        y[i]=a+i*(b-a)/N
    
    return y

#to define the function to calculate pressure
def pressure(y,variable):
    pa=variable['pa']
    pb=variable['pb']
    L=variable['L']
    p=np.zeros(len(y))
    p=pa+pb*np.cos(y*np.pi/L)
    
    return p

#the function to calculate dp/dy with 2nd order finite difference formula 
def twopoint_gradient(f,dx):
    dfdx=np.zeros_like(f)
    #two end points using forward and backward differences
    dfdx[0]=(f[1]-f[0])/dx
    dfdx[-1]=(f[-1]-f[-2])/dx
    #using centred, 2nd order finite difference formula
    for i in range(1,len(f)-1):
        dfdx[i]=(f[i+1]-f[i-1])/(2*dx)
        
    return dfdx


#define the function of analytical pressure gradient   
def exact_gradient(y,variable):
    pb=variable['pb']
    L=variable['L']
    p=np.zeros(len(y))
    p=(pb*np.pi/L)*(-np.sin(y*np.pi/L))
    
    return p

#the followting two functions are to calculate the analytical and numerical solution of speed
def u_exact(y,variable):
    pb=variable['pb']
    L=variable['L']
    ro=variable['ro']
    f=variable['f']
    
    return pb*np.pi/(ro*f*L)*np.sin(y*np.pi/L)

def u_numerical(dpdy,variable):
    ro=variable['ro']
    f=variable['f']
    
    return -dpdy/(ro*f)

#to calculate the errors between numerical and analytical solutions 
def errors(evaluation,exact):
    
    return evaluation - exact

#the function to calculate dp/dy with 3rd order finite difference formula 
def threepoint_gradient(f,dx):
    dfdx=np.zeros_like(f)
    #two end points using forward and backward differences and 2nd order finite difference formula
    dfdx[-2]=(f[-1]-f[-3])/(2*dx)
    dfdx[-1]=(f[-1]-f[-2])/dx
    #using uncentred, 3rd order finite difference formula
    for i in range(1,len(f)-2):
        dfdx[i]=(-f[i+2]+4*f[i+1]-3*f[i])/(2*dx)
        
    return dfdx
#to find a more accurate way to get numerical soltion, using 3rd order finite difference formula
    

#give a dictionary with all provided variables
pro_variable={'pa'   :10**5,
              'pb'   :200,
              'f'    :10**(-4),
              'ro'   :1,
              'L'    :2.4*10**6,
              'y_min':0,
              'y_max':10**6}
#the domain is divided into N = 10 equal intervals
    
N=10

y=y_group(N,pro_variable)
p=pressure(y,pro_variable)
dpdy=twopoint_gradient(p,y[-1]/N)
dpdy_exact=exact_gradient(y,pro_variable)

eu1=u_exact(y,pro_variable)
nu1=u_numerical(dpdy,pro_variable)

e1=errors(nu1,eu1)

fig = plt.figure(figsize = (13,5))
ax1 = fig.add_subplot(1,2,1)  # set the fist graph
ax1.plot(y,nu1,label='numerical solutions')
ax1.plot(y,eu1,label='analytical solutions')
plt.xlabel('space locations/m')
plt.ylabel('speed(m/s)')
plt.title('the numerical and analytical solutions for N = 10')
ax1.legend()
#plot the errors of 2nd and 3rd difference fomula
ax2 = fig.add_subplot(1,2,2)  # set the fist graph
ax2.plot(y, e1, 'b-',label = 'errors')
ax2.axhline(y=0,color='r',linestyle='--')
plt.xlabel('space locations/m')
plt.ylabel('speed(m/s)')
plt.title('Errors for N = 10')
plt.legend()
plt.show()

#for question 2, in order to find a more accurate solution, using 3rd order finite difference fomula, for N = 100

N=50

y=y_group(N,pro_variable)
p=pressure(y,pro_variable)
dpdy1=twopoint_gradient(p,y[-1]/N)
dpdy2=threepoint_gradient(p,y[-1]/N)
dpdy_exact=exact_gradient(y,pro_variable)

eu2=u_exact(y,pro_variable)
nu2=u_numerical(dpdy1,pro_variable)
nu2_=u_numerical(dpdy2,pro_variable)

#calculate the error for numerical and analytical solutions, in order to give the comparison later
e2=errors(nu2,eu2)
e3=errors(nu2_,eu2)


fig = plt.figure(figsize = (13,5))
ax1 = fig.add_subplot(1,2,1)  # set the fist graph
ax1.plot(y,nu2,label='two points')
ax1.plot(y,nu2_,label='three points')
ax1.plot(y,eu2,label='analytical solutions')
plt.xlabel('space locations/m')
plt.ylabel('speed(m/s)')
plt.title('Two numeriacl and an analytical solutions for N = 50')
ax1.legend()
#plot the errors of 2nd and 3rd difference fomula
ax2 = fig.add_subplot(1,2,2)  # set the second graph
ax2.plot(y, e2, 'k-',label = '2nd order',linestyle='dashdot')
ax2.plot(y, e3, 'y-',label = '3rd order')
ax2.axhline(y=0,color='r',linestyle='--')
plt.xlabel('space locations/m')
plt.ylabel('speed(m/s)')
plt.title('Errors for the two ways while N = 50')
plt.legend()
plt.show()


#to see if the errors of solutions of these two ways are still similar when N is smaller

N=10

y=y_group(N,pro_variable)
p=pressure(y,pro_variable)
dpdy1=twopoint_gradient(p,y[-1]/N)
dpdy2=threepoint_gradient(p,y[-1]/N)
dpdy_exact=exact_gradient(y,pro_variable)

eu2=u_exact(y,pro_variable)
nu2=u_numerical(dpdy1,pro_variable)
nu2_=u_numerical(dpdy2,pro_variable)

#calculate the error for numerical and analytical solutions, in order to give the comparison later
e2=errors(nu2,eu2)
e3=errors(nu2_,eu2)


fig = plt.figure(figsize = (13,5))
ax1 = fig.add_subplot(1,2,1)  # set the fist graph
ax1.plot(y,nu2,label='two points')
ax1.plot(y,nu2_,label='three points')
ax1.plot(y,eu2,label='analytical solutions')
plt.xlabel('space locations/m')
plt.ylabel('speed(m/s)')
plt.title('Two numerical and an analytical solutions for N = 10')
ax1.legend()
#plot the errors of 2nd and 3rd difference fomula
ax2 = fig.add_subplot(1,2,2)  # set the second graph
ax2.plot(y, e2, 'k-',label = '2nd order',linestyle='dashdot')
ax2.plot(y, e3, 'y-',label = '3rd order')
ax2.axhline(y=0,color='r',linestyle='--')
plt.xlabel('space locations/m')
plt.ylabel('speed(m/s)')
plt.title('Errors for the two ways while N = 10')
plt.legend()
plt.show()












